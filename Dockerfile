#Docker base image
FROM openjdk:8-jre-alpine

#Check java version
#RUN ["java", "-version"]

#RUN apt-get update

EXPOSE 8080/tcp

COPY ./build/libs/jb-hello-world-0.1.0.jar app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]
